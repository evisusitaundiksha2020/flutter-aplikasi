import 'package:flutter/material.dart';

void main() {
  runApp(HitungLuasSegitiga());
}

class HitungLuasSegitiga extends StatefulWidget {
  @override
  _HitungLuasSegitigaState createState() => _HitungLuasSegitigaState();
}

class _HitungLuasSegitigaState extends State<HitungLuasSegitiga> {
  double alas = 0;
  double tinggi = 0;
  double luas = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Menghitung Luas Segitiga"),
            Row(
              children: [
                Text("Alas"),
                Expanded(
                  child: TextField(
                    onChanged: (txt) {
                      setState(() {
                        alas = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Text("Tinggi"),
                Expanded(
                  child: TextField(
                    onChanged: (txt) {
                      setState(() {
                        tinggi = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                  ),
                )
              ],
            ),
            RaisedButton(
              onPressed: () {
                setState(() {
                  luas = 0.5 * alas * tinggi;
                });
              },
              child: Text("HITUNG!"),
            ),
            Text("Luas Segitiga Alas =$alas, Tinggi =$tinggi"),
            Text("$luas")
          ],
        ),
      ),
    );
  }
}